<?php

namespace RDP\Http\Controllers;

use Illuminate\Http\Request;
use RDP\Models\Transaction;
use RDP\Models\Address;
use RDP\Models\Customer;

use Session;

/**
 * Class TransactionController
 * @package RDP\Http\Controllers
 *
 * This handles communication between checkout view and Transaction Model
 */
class TransactionController extends Controller
{
    /*========== Functions for pages =========== --*/

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * This shows checkout view page
     */
    public function checkout(){
      $data = [
        'module' => 'checkout'
      ];
      return view('shop/checkout', $data);
    }

    /*========== End of Functions for pages =========== --*/

    /*========== Functions for actions =========== --*/
    /**
     * @param Request $request
     * @return Transaction
     */
    public function add(Request $request){
      $result = new Transaction($request);

      return $result;
    }

    /**
     * @param Request $request
     * @return Address
     */
    public function addAddress(Request $request){
      $result = new Address($request);

      return $result;
    }

    /**
     * @param Request $request
     * @return Customer
     */
    public function addCustomer(Request $request){
      $result = new Customer($request);

      return $result;
    }
    /*========== End of Functions for actions =========== --*/
}
