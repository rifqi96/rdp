<?php

namespace RDP\Http\Controllers;

use Illuminate\Http\Request;

use RDP\Models\Product;

/**
 * Class ShoppingPageController
 * @package RDP\Http\Controllers
 *
 * This controller only to show indexsite
 */
class ShoppingPageController extends Controller
{
    /*========== Functions for pages =========== --*/
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * Function to show the index site
     *
     * passing Product object
     */
    public function index(){
      $Product = new Product;

      $products = $Product->getAll();
      $data = [
        'module' => 'shopping',
        'products' => $products
      ];

      return view('shop/shoppingpage', $data);
    }
    /*========== End of Functions for pages =========== --*/
}
