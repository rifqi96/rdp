<?php

namespace RDP\Http\Controllers;

use Illuminate\Http\Request;
use RDP\Models\Cart;
use RDP\Models\Product;
use Session;


/**
 * Class CartController
 * @package RDP\Http\Controllers
 * This handles communication between Cart Model and all views.
 * This controller uses Session. So, everytime Cart Object is made, it is stored into Session.
 */
class CartController extends Controller
{
    /**
     * @param Request $request
     * @return array
     */
    public function add(Request $request){
      $Products = new Product;
      $product = $Products->getById($request->id);
      $oldCart = Session::has('cart') ? Session::get('cart') : null;
      $cart = new Cart($oldCart);
      $res = 'false';

      if($cart->doesExists($request->id)){
        if(($product->qty - $cart->getItemQty($request->id))<1){
          $result = array(
            'status' => '0',
            'message' => 'Failed to add item. Stock is not sufficient.',
            'session' => array(
              'items' => Session::get('cart')->getAllItems(),
              'totalPrice' => Session::get('cart')->getTotalPrice(),
              'totalQty' => Session::get('cart')->getTotalQty()
            )
          );
        }
        else{
          $cart->add($product, $product->id);

          $request->session()->put('cart', $cart);

          $result = array(
            'status' => '1',
            'message' => 'Item added to a cart',
            'session' => array(
              'items' => Session::get('cart')->getAllItems(),
              'totalPrice' => Session::get('cart')->getTotalPrice(),
              'totalQty' => Session::get('cart')->getTotalQty()
            )
          );
        }
      }
      else{
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);

        $result = array(
          'status' => '1',
          'message' => 'Item added to a cart',
          'session' => array(
            'items' => Session::get('cart')->getAllItems(),
            'totalPrice' => Session::get('cart')->getTotalPrice(),
            'totalQty' => Session::get('cart')->getTotalQty()
          )
        );
      }

      return $result;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function update(Request $request){
      $result = array(
        'status' => '0',
        'message' => 'Failed to update. No Cart.',
      );

      if(Session::has('cart')){
        $cart = Session::get('cart', []);
        $Products = new Product;
        $product = $Products->getById($request->id);
        $productQty = $product->qty;

        if($request->qty>$productQty || $request->qty<1 || $request->qty=='' || $request->qty==null || !isset($request->qty)){
          $result = array(
            'status' => '0',
            'message' => 'Item stock is not sufficient',
            'session' => array(
              'items' => Session::get('cart')->getAllItems(),
              'totalPrice' => Session::get('cart')->getTotalPrice(),
              'totalQty' => Session::get('cart')->getTotalQty()
            )
          );
        }
        else{
          $cart->setQty($request->id, $request->qty);

          $result = array(
            'status' => '1',
            'message' => 'Quantity updated',
            'session' => array(
              'items' => Session::get('cart')->getAllItems(),
              'totalPrice' => Session::get('cart')->getTotalPrice(),
              'totalQty' => Session::get('cart')->getTotalQty()
            )
          );
        }
      }
      return $result;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function remove(Request $request){
      $result = array(
        'status' => '0',
        'message' => 'Failed to delete. No Cart.',
      );

      if(Session::has('cart')){
        $cart = Session::get('cart');

        $cart->remove($request->id);

        $result = array(
          'status' => '1',
          'message' => 'Item has been removed',
          'session' => array(
            'items' => Session::get('cart')->getAllItems(),
            'totalPrice' => Session::get('cart')->getTotalPrice(),
            'totalQty' => Session::get('cart')->getTotalQty()
          )
        );
      }
      return $result;
    }

    /**
     * @return array
     */
    public function destroy(){
      $result = array(
        'status' => '0',
        'message' => 'Failed to delete. No Cart.',
      );

      if(Session::has('cart')){
        $cart = Session::get('cart');

        $cart->destroy();

        Session::forget('cart');

        $result = array(
          'status' => '1',
          'message' => 'Cart has been removed'
        );
      }
      return $result;
    }
}
