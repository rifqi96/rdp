<?php

namespace RDP\Http\Controllers;

use Illuminate\Http\Request;
use RDP\Models\Receipt;

use Session;

/**
 * Class ReceiptController
 * @package RDP\Http\Controllers
 *
 * This controller only to show receipt page
 */
class ReceiptController extends Controller
{
  /*========== Functions for pages =========== --*/
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * Function to show the receipt
     *
     * Passing Receipt Session
     */
    public function show($id){
    if(Session::has('receipt') && Session::get('receipt')->getAll()['transaction_id']==$id){
      $data = [
        'module' => 'receipt',
        'receipt' => Session::get('receipt')->getAll()
      ];
    }
    else{
      $data = [
        'module' => 'receipt',
        'receipt' => false
      ];
    }
    return view('shop/receipt', $data);
  }
  /*========== End of Functions for pages =========== --*/
}
