<?php

namespace RDP\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Customer
 * @package RDP\Models
 *
 * Its attributes are generated from database table 'customers' using Eloquent Model
 *
 */
class Customer extends Model
{
    /**
     * Customer constructor.
     * @param string $customer
     */
    public function __construct($customer=''){
    if($customer){
      $this->address_id = $customer->address_id;
      $this->name = $customer->name;
      $this->email = $customer->email;

      return json_encode($this->save());
    }
  }

    /**
     * @param $id
     * @return Model|null|static
     */
    public function getByIdWithAddress($id){
    return $this->select('*')
    ->join('customer_address','customer_address.id','=','customers.address_id')
    ->where('customers.id', $id)
    ->first();
  }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll(){
    return $this->all();
  }

    /**
     * @param $id
     * @return Model|null|static
     */
    public function getById($id){
    return $this->where('id', $id)->first();
  }
}
