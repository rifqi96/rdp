<?php

namespace RDP\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Session;

/**
 * Class Transaction
 * @package RDP\Models
 * This is Transaction class. This class handles transaction data insert into the transaction table.
 */
class Transaction extends Model
{
    /**
     * Transaction constructor.
     * @param array $transaction
     */
    public function __construct($transaction){
    $result = array();

        /**
         * Insert new transaction
         */
    $this->customer_id = $transaction->customer_id;
    $this->token = $transaction->token;
    $this->payment_type = $transaction->payment_type;
    $this->card_id = $transaction->card_id;
    $this->client_ip = $transaction->client_ip;

    $transaction = $this->save();

        /**
         * Insert transaction_details
         */
    $this->addDetails(Session::get('cart')->getAllItems());

        /**
         * Decrease product stock
         */
    $this->decreaseStock(new Product);

        /**
         * Add Receipt
         */
    $this->addReceipt(new Customer);

    return json_encode($transaction);
  }

    /**
     * @param $Customer
     */
    public function addReceipt($Customer){
    $address = $Customer->getByIdWithAddress($this->customer_id);

    $data = array(
      'transaction_id' => $this->id,
      'address' => $address,
      'order' => $this->getDetails($this->id),
      'totalPrice' => Session::get('cart')->getTotalPrice()
    );

    $receipt = new Receipt($data);

    Session::put('receipt', $receipt);
  }

    /**
     * @param $id
     * @return mixed
     */
    public function getDetails($id){
    return DB::table('transaction_details')
    ->select('products.name as product_name', 'products.price as product_price', 'transaction_details.qty as qty', 'transaction_details.price as price')
    ->join('products', 'products.id', '=', 'transaction_details.product_id')
    ->where('transaction_details.transaction_id', $id)
    ->get();
  }

    /**
     * @param $CartAllItems
     */
    public function addDetails($CartAllItems){
    $data = array();

    foreach($CartAllItems as $cart){
      array_push($data, [
        'transaction_id' => $this->id,
        'product_id' => $cart['item']['id'],
        'qty' => $cart['qty'],
        'price' => $cart['price']
      ]);
    }
    DB::table('transaction_details')->insert($data);
  }

    /**
     * @param $Product
     */
    public function decreaseStock($Product){
    $products = $Product->getAll();
    foreach($products as $product){
      foreach(Session::get('cart')->getAllItems() as $cart){
        if($product->id === $cart['item']['id']){
          $Product->setQty($product->id, ($product->qty - $cart['qty']));
        }
      }
    }
  }
}
