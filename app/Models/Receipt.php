<?php

namespace RDP\Models;

/**
 * Class Receipt
 * @package RDP\Models
 *
 * This class generates receipt when the transaction has been made
 */
class Receipt
{
    /**
     * @var integer
     */
    private $transaction_id;
    /**
     * @var array
     */
    private $address;
    /**
     * @var array
     */
    private $order;
    /**
     * @var integer
     */
    private $totalPrice;

    /**
     * Receipt constructor.
     * @param array $data
     */
    public function __construct($data=''){
    if($data){
      $this->transaction_id = $data['transaction_id'];
      $this->address = $data['address'];
      $this->order = $data['order'];
      $this->totalPrice = $data['totalPrice'];
    }
  }

    /**
     * @return array
     */
    public function getAll(){
    $result = array(
      'transaction_id' => $this->transaction_id,
      'address' => $this->address,
      'order' => $this->order,
      'totalPrice' => $this->totalPrice
    );

    return $result;
  }
}
