<?php

namespace RDP\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Address
 * @package RDP\Models
 * Its main job is to insert into customer_address
 */
class Address extends Model
{
    /**
     * @var string
     */
    protected $table = 'customer_address';

    /**
     * Address constructor.
     * @param string $customer_address
     */
    public function __construct($customer_address=''){
      if($customer_address){
        $this->address = $customer_address->address;
        $this->city = $customer_address->city;
        $this->country = $customer_address->country;
        $this->postal_code = $customer_address->postal_code;

        return json_encode($this->save());
      }
    }

    /**
     * @param $id
     * @return Model|null|static
     */
    public function getById($id){
      return $this->where('id', $id)->first();
    }
}
