<?php

namespace RDP\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Class Product
 * @package RDP\Models
 *
 * This is Product class. Its attributes are generated from database table 'products' by Eloquent Model
 */

class Product extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll(){
    return $this->all();
  }

    /**
     * @param $id
     * @return Model|null|static
     */
    public function getById($id){
    return $this->where('id', $id)->first();
  }

    /**
     * @param $id
     * @param $qty
     * @return bool
     */
    public function setQty($id, $qty){
    return $this->where('id', $id)
    ->update([
      'qty' => $qty
    ]);
  }
}
