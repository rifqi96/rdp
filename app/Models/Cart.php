<?php

namespace RDP\Models;

/**
 * Class Cart
 * @package RDP\Models
 * This is Cart Class. It handles every setters and getters, add new cart, remove cart, etc.
 */
class Cart
{
    /**
     * @var null
     */
    private $items = null;
    /**
     * @var int
     */
    private $totalPrice = 0;
    /**
     * @var int
     */
    private $totalQty = 0;

    /**
     * Cart constructor.
     * @param $oldCart
     */
    public function __construct($oldCart){
      if($oldCart){
        $this->items = $oldCart->items;
        $this->totalQty = $oldCart->totalQty;
        $this->totalPrice = $oldCart->totalPrice;
      }
    }

    /**
     * @param $item
     * @param $id
     * Append new item into cart object. Calculate new qty and price.
     */
    public function add($item, $id){
      $storedItem = [
        'qty' => 0,
        'price' => $item->price,
        'item' => $item
      ];
      
      if($this->doesExists($id)){
        $storedItem = $this->items[$id];
      }

      $storedItem['qty']++;
      $storedItem['price'] = $item->price * $storedItem['qty'];
      $this->items[$id] = $storedItem;
      $this->totalQty++;
      $this->totalPrice+=$item->price;
    }

    /**
     * @param $id
     * @return bool
     * Check if cart exists
     */
    public function doesExists($id){
      if($this->items){
        if(array_key_exists($id, $this->items)){
          return true;
        }
        else{
          return false;
        }
      }
      return false;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getItemQty($id){
      return $this->items[$id]['qty'];
    }

    /**
     * @return int
     */
    public function getTotalPrice(){
      return $this->totalPrice;
    }

    /**
     * @return int
     */
    public function getTotalQty(){
      return $this->totalQty;
    }

    /**
     * @return null
     */
    public function getAllItems(){
      return $this->items;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id){
      if($this->items){
        if(array_key_exists($id, $this->items)){
          return $this->items[$id];
        }
      }
    }

    /**
     * @param $id
     * @param $qty
     */
    public function setQty($id, $qty){
      if($this->items){
        if(array_key_exists($id, $this->items)){
          $this->totalPrice = $this->totalPrice - $this->items[$id]['price'];
          $this->totalQty = $this->totalQty - $this->items[$id]['qty'];
          $this->items[$id]['qty']=$qty;
          $this->items[$id]['price'] = $this->items[$id]['item']['price'] * $qty;
          $this->totalQty = $this->totalQty + $this->items[$id]['qty'];
          $this->totalPrice = $this->totalPrice + $this->items[$id]['price'];
        }
      }
    }

    /**
     * @param $id
     * Remove one cart session by id
     */
    public function remove($id){
      if($this->items){
        if(array_key_exists($id, $this->items)){
          $this->totalPrice = $this->totalPrice - $this->items[$id]['price'];
          $this->totalQty = $this->totalQty - $this->items[$id]['qty'];
          unset($this->items[$id]);
        }
      }
    }

    /**
     * Remove the whole cart session
     */
    public function destroy(){
      if($this->items){
        foreach($this->items as $key => $value){
          unset($this->items[$key]);
        }
        $this->totalPrice = 0;
        $this->totalQty = 0;
      }
    }
}
