<?php

use Illuminate\Database\Seeder;

class CreateProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = array();
      for($i=1; $i<=5; $i++){
        array_push($data, [
          'id'=>$i,
          'name'=>'Product '.$i,
          'price'=>rand(25,100),
          'qty'=>rand(10,50),
          'image'=>'shop/images/img_'.$i.'.png'
        ]);
      }

      DB::table('products')->insert($data);
    }
}
