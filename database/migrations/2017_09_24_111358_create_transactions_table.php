<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('transactions', function(Blueprint $table){
        $table->increments('id');
        $table->integer('customer_id')->unsigned();
        $table->string('token');
        $table->string('payment_type');
        $table->string('card_id');
        $table->ipAddress('client_ip');
        $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('updated_at')->nullable();
      });

      Schema::table('transactions', function(Blueprint $table){
        $table->foreign('customer_id')
        ->references('id')
        ->on('customers')
        ->onDelete('NO ACTION')
        ->onUpdate('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
