<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('transaction_details', function(Blueprint $table){
        $table->increments('id');
        $table->integer('transaction_id')->unsigned();
        $table->integer('product_id')->unsigned();
        $table->integer('qty');
        $table->integer('price');
        $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('updated_at')->nullable();
      });

      Schema::table('transaction_details', function(Blueprint $table){
        $table->foreign('transaction_id')
        ->references('id')
        ->on('transactions')
        ->onDelete('NO ACTION')
        ->onUpdate('CASCADE');

        $table->foreign('product_id')
        ->references('id')
        ->on('products')
        ->onDelete('NO ACTION')
        ->onUpdate('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
