@extends('shop.layouts.master')

@section('title')
Shopping Page
@endsection

@section('content')
<div class="row">
  @foreach($products as $product)
    <div class="col-md-3 text-center">
      <img src="{{asset($product->image)}}" width="150px" height="150px">
      <br>
      {{$product->name}} - <strong>${{$product->price}}</strong>
      <br>
      Quantity - <strong>{{$product->qty<0?0:$product->qty}}</strong>
      <br>
      <button class="btn btn-danger my-cart-btn"
      data-id="{{$product->id}}"
      data-name="{{$product->name}}"
      data-price="{{$product->price}}"
      data-quantity="1"
      data-image="{{asset($product->image)}}">Add to Cart</button>
    </div>
  @endforeach
</div>

<!-- Cart Modal -->
<div class="modal fade" id="my-cart-modal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Your Shopping Cart</h4>
      </div>
      <form action="{{route('transaction.checkout')}}" method="post">
        {{csrf_field()}}
        <div class="modal-body">
          @if(Session::has('cart') && count(Session::get('cart')->getAllItems())>0)
          <table class="table table-resonsive table-hover my-cart-table">
            <tr>
              <th></th>
              <th>Product Name</th>
              <th>Price</th>
              <th>Qty</th>
              <th>Sum</th>
              <th></th>
            </tr>
            @foreach(Session::get('cart')->getAllItems() as $value)
            <tr data-id="{{$value['item']['id']}}">
              <td><img width="30px" height="30px" src="{{$value['item']['image']}}" style='"position":"fixed"; "z-index": "999";' alt=""></td>
              <td>{{$value['item']['name']}}</td>
              <td>${{$value['item']['price']}}</td>
              <td><input class="my-product-quantity" type="number" name="qty" min="1" max="{{$value['item']['qty']}}" value="{{$value['qty']}}"></td>
              <td class="my-product-sum"><b>${{$value['item']['price']*$value['qty']}}</b></td>
              <td><button type="button" name="remove" class="btn btn-sm btn-danger my-product-remove">X</button></td>
            </tr>
            @endforeach
            <tr>
              <td colspan="4"><b>Total</b></td>
              <td colspan="2" class='my-cart-total'><b>${{Session::get('cart')->getTotalPrice()}}</b></td>
            </tr>
          </table>
          @else
          <div class="well">
            Please add some item to cart first
          </div>
          @endif
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Continue Shopping</button>
          @if(Session::has('cart') && count(Session::get('cart')->getAllItems())>0)
          <button type="submit" class="btn btn-info">Checkout</button>
          @endif
        </div>
      </form>
    </div>
  </div>
</div>
<!-- End of Cart Modal -->

<script type="text/javascript">

$(function () {
  // ========= Begin Click Add To Cart Event Handler =========//
  $('.my-cart-btn').on('click', function(){
    var data = $(this).data();
    data['_token'] = '{{csrf_token()}}';

    $.post({
      url:'{{route("cart.add")}}',
      data:data,
      success:function(e){
        if(!e.error){
          if(e.status==1){
            location.reload(true);
          }
          else{
            alert('Error! ' + e.message);
          }
        }
      }
    });

  });
  // ========= End of Click Add To Cart Event Handler =========//

  // ======= WHEN QUANTITY IS CHANGING ======== //
  $('.my-cart-icon').on('click', function(){
    $('.my-product-quantity').bind('keyup mouseup', function(e){
      var id = $(this).closest('tr').attr('data-id');
      var $sum = $(this).closest('tr').find('.my-product-sum b');
      var $total = $(this).closest('table').find('.my-cart-total b');
      var $cartBadge = $(document).find('.my-cart-icon .my-cart-badge');
      var qty = $(this).val();

      var data = {
        id:id,
        qty:qty,
        _token:'{{csrf_token()}}'
      };

      $.post({
        url:'{{route("cart.update")}}',
        data:data,
        success:function(e){
          if(!e.error){
            if(e.status==1){
              if(e.session.items[id]){
                $sum.text('$' + e.session.items[id]['price']);
                $total.text('$' + e.session.totalPrice);
                $cartBadge.text(e.session.totalQty);
              }
            }else{
              alert('Error! ' + e.message);
            }
          }
        }
      });
    });
  });
  // ======= END OF WHEN QUANTITY IS CHANGING ======== //

  // ======= WHEN REMOVE BUTTON IS CLICKED ======== //
  $('.my-product-remove').on('click', function(e){
    var id = $(this).closest('tr').attr('data-id');
    var $tr = $(this).closest('tr');
    var $total = $(this).closest('table').find('.my-cart-total b');
    var $cartBadge = $(document).find('.my-cart-icon .my-cart-badge');

    var data = {
      id:id,
      _token:'{{csrf_token()}}'
    };

    $.post({
      url:'{{route("cart.remove")}}',
      data:data,
      success:function(e){
        if(!e.error){
          if(e.status==1){
            $total.text('$' + e.session.totalPrice);
            $cartBadge.text(e.session.totalQty);
            $tr.remove();
            if(e.session.items.length<1){
              location.reload(true);
            }
          }
          else{
            alert('Error! ' + e.message);
          }
        }
      }
    });
  });
  // ======= END OF WHEN REMOVE BUTTON IS CLICKED ======== //
});

</script>
@endsection
