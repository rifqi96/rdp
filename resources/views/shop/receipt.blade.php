@extends('shop.layouts.master')

@section('title')
@if(Session::has('receipt') && $receipt!=false)
Invoice for purchase #{{$receipt['transaction_id']}}
@else
No Invoice
@endif
@endsection

@section('content')
@if(Session::has('receipt') && $receipt!=false)
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12 col-md-3 col-lg-3 pull-left">
                <div class="panel panel-default height">
                    <div class="panel-heading">Address Details</div>
                    <div class="panel-body">
                      <strong>{{$receipt['address']['name']}}:</strong><br>
                      {{$receipt['address']['address']}}<br>
                      {{$receipt['address']['city']}}<br>
                      {{$receipt['address']['country']}}<br>
                      <strong>{{$receipt['address']['postal_code']}}</strong><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-center"><strong>Order summary</strong></h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <td><strong>Item Name</strong></td>
                                <td class="text-center"><strong>Item Price</strong></td>
                                <td class="text-center"><strong>Item Quantity</strong></td>
                                <td class="text-right"><strong>Total</strong></td>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($receipt['order'] as $order)
                            <tr>
                                <td>{{$order->product_name}}</td>
                                <td class="text-center">${{$order->product_price}}</td>
                                <td class="text-center">{{$order->qty}}</td>
                                <td class="text-right">${{$order->price}}</td>
                            </tr>
                          @endforeach
                            <tr>
                                <td class="emptyrow"><i class="fa fa-barcode iconbig"></i></td>
                                <td class="emptyrow"></td>
                                <td class="emptyrow text-center"><strong>Total</strong></td>
                                <td class="emptyrow text-right">${{$receipt['totalPrice']}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<style>
.height {
    min-height: 200px;
}

.icon {
    font-size: 47px;
    color: #5CB85C;
}

.iconbig {
    font-size: 77px;
    color: #5CB85C;
}

.table > tbody > tr > .emptyrow {
    border-top: none;
}

.table > thead > tr > .emptyrow {
    border-bottom: none;
}

.table > tbody > tr > .highrow {
    border-top: 3px solid;
}
</style>
@endsection
