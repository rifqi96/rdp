@extends('shop.layouts.master')

@section('title')
Payment Gateway
@endsection

@section('content')

<div class="container">
    <div class='row'>
        <div class='col-md-4'></div>
        <div class='col-md-4'>
          <script src='https://js.stripe.com/v3/' type='text/javascript'></script>
          <script src="https://checkout.stripe.com/checkout.js"></script>

          <table class='table table-hover table-responsive'>
            <tr>
              <th>ID</th>
              <th>Item Name</th>
              <th>Item Price</th>
              <th>Item Quantity</th>
              <th>Total</th>
            </tr>
            @if(Session::get('cart') && count(Session::get('cart'))>0)
              @foreach(Session::get('cart')->getAllItems() as $value)
                <tr>
                  <td>{{$value['item']['id']}}</td>
                  <td>{{$value['item']['name']}}</td>
                  <td>${{$value['item']['price']}}</td>
                  <td>{{$value['qty']}}</td>
                  <td><b>${{$value['price']}}</b></td>
                </tr>
              @endforeach
            <tr>
              <td colspan='4'><b>You must pay</b></td>
              <td><b>${{Session::get('cart')->getTotalPrice()}}</b></td>
            </tr>
            @endif

            <tr>
              <td colspan='4'><button class="btn btn-default" onclick="window.history.back();">Back</button></td>
              <td><button class="btn btn-success" id="payButton">Pay</button></td>
            </tr>
          </table>
        </div>
        <div class='col-md-4'></div>
    </div>
</div>

<script type="text/javascript">
function removeCartSess(){
  $.get("{{route('cart.destroy')}}", function(data){});
}

var handler = StripeCheckout.configure({
  key: '{{config("app.stripe.published_key")}}',
  image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
  locale: 'auto',
  shippingAddress:true,
  billingAddress:true,
  token: function(token) {
    // You can access the token ID with `token.id`.
    // Get the token ID to your server-side code for use.

    if(token){
      var address_id="", customer_id="", transaction_id="";

      var address = {
        address:token.card.address_line1,
        city:token.card.address_city,
        country:token.card.address_country,
        postal_code:token.card.address_zip,
        _token:'{{csrf_token()}}'
      };

      $.post({
        url:'{{route("transaction.add.address")}}',
        data:address,
        success:function(result){
          address_id = result.id;
        },
        async:false
      });

      var customer = {
        address_id:address_id,
        name:token.card.name,
        email:token.email,
        _token:'{{csrf_token()}}'
      };

      $.post({
        url:'{{route("transaction.add.customer")}}',
        data:customer,
        success:function(result){
          customer_id = result.id;
        },
        async:false
      });

      var transaction = {
        customer_id:customer_id,
        token:token.id,
        payment_type:token.card.object,
        card_id:token.card.id,
        client_ip:token.client_ip,
        _token:'{{csrf_token()}}'
      };

      $.post({
        url:'{{route("transaction.add")}}',
        data:transaction,
        success:function(result){
          transaction_id = result.id;
        },
        async:false
      });

      removeCartSess();

      window.location.href= "<?php echo url('/') ?>" + "/transaction/receipt/"+transaction_id;
    }
  }
});

var amount = '{{Session::get("cart")->getTotalPrice()}}';
var description = '{{Session::get("cart")->getTotalQty()}} items purchase';

document.getElementById('payButton').addEventListener('click', function(e) {
  // Open Checkout with further options:
  handler.open({
    name: 'RDP',
    description: description,
    amount: amount*100 //in form of cents.
  });
  e.preventDefault();
});

// Close Checkout on page navigation:
window.addEventListener('popstate', function() {
  handler.close();
});

</script>
@endsection
