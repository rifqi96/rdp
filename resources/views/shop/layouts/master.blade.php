<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('app.name')}} | @yield('title')</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Bootstrap -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous"> -->

    <link rel="stylesheet" href="{{asset('shop/css/bootstrap.min.css')}}">
    <style>
    .badge-notify{
      background:red;
      position:relative;
      top: -20px;
      right: 10px;
    }
    .my-cart-icon-affix {
      position: fixed;
      z-index: 999;
    }
    </style>
  </head>
  <script src="{{asset('shop/js/jquery-2.2.3.min.js')}}"></script>
  <script type='text/javascript' src="{{asset('shop/js/bootstrap.min.js')}}"></script>

  <body class="container">
    <div class="page-header">
      <div class="text-center">
        <h1>
          @yield('title')
          @if($module=='shopping')
          <div style="float: right; cursor: pointer;"><span class="glyphicon glyphicon-shopping-cart my-cart-icon" data-toggle="modal" data-target="#my-cart-modal">@if(Session::has('cart') && count(Session::get('cart')->getAllItems())>0)<span class="badge badge-notify my-cart-badge">{{Session::get('cart')->getTotalQty()}}</span>@endif</span></div>
          @elseif($module=='receipt')
          <div style="float: left; cursor: pointer;"><button class="btn btn-lg btn-success" type="button" onclick="window.location.href='{{url('/')}}';"><span class="glyphicon glyphicon-home"></span></button></div>
          @endif
        </h1>
      </div>
    </div>
    <div class="container">
      @yield('content')
    </div>
  </body>
</html>
