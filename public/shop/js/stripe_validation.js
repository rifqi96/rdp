function removeCartSess(){
  $.get("{{route('cart.destroy')}}", function(data){});
}

var handler = StripeCheckout.configure({
  key: '{{config("app.stripe.published_key")}}',
  image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
  locale: 'auto',
  shippingAddress:true,
  billingAddress:true,
  token: function(token) {
    // You can access the token ID with `token.id`.
    // Get the token ID to your server-side code for use.

    if(token){
      console.log(token);

      var address_id="", customer_id="";

      var address = {
        address:token.card.address_line1,
        city:token.card.address_city,
        country:token.card.address_country,
        postal_code:token.card.address_zip
      };


      $.post({
        url:'{{route("transaction.add.address")}}',
        data:address,
        success:function(result){
          address_id = result.id;
        },
        async:false
      });

      alert('nice');
      alert(address_id);

      // removeCartSess();
      // window.location.href="{{route('transaction.receipt', ['id'=>'11'])}}";
    }

    // var customer = {
    //   //address_id:address_id,
    //   name:token.card.name,
    //   email:token.email
    // };

    // $.post({
    //   url:'{{route("transaction.add.customer")}}',
    //   data:data,
    //   success:function(result){
    //     customer_id = result.id;
    //   },
    //   async:false
    // });

    // var transaction = {
    //   //customer_id:customer_id,
    //   token:token.id,
    //   payment_type:token.card.object,
    //   card_id:token.card.id,
    //   client_ip:token.client_ip
    // };

    // $.post({
    //   url:'{{route("transaction.add")}}',
    //   data:data,
    //   success:function(result){
    //     alert('Success');
    //   },
    //   async:false
    // });
  }
});

var amount = '{{Session::get("cart")->getTotalPrice()}}';
var description = '{{Session::get("cart")->getTotalQty()}} items purchase';

document.getElementById('payButton').addEventListener('click', function(e) {
  // Open Checkout with further options:
  handler.open({
    name: 'RDP',
    description: description,
    amount: amount*100
  });
  e.preventDefault();
});

// Close Checkout on page navigation:
window.addEventListener('popstate', function() {
  handler.close();
});
