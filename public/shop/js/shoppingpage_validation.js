$(function () {
  $(document).find('.modal .modal-body .alert .alert-danger').hide();

  // ========= Begin Click Add To Cart Event Handler =========//
  $('.my-cart-btn').on('click', function(){
    var data = $(this).data();
    data['_token'] = '{{csrf_token()}}';

    $.post({
      url:'{{route("cart.add")}}',
      data:data,
      success:function(e){
        location.reload(true);
      }
    });

  });
  // ========= End of Click Add To Cart Event Handler =========//

  // ======= WHEN QUANTITY IS CHANGING ======== //
  $('.my-cart-icon').on('click', function(){
    $('.my-product-quantity').bind('keyup mouseup', function(e){
      var id = $(this).closest('tr').attr('data-id');
      var $sum = $(this).closest('tr').find('.my-product-sum b');
      var $total = $(this).closest('table').find('.my-cart-total b');
      var $cartBadge = $(document).find('.my-cart-icon .my-cart-badge');
      var qty = $(this).val();

      var data = {
        id:id,
        qty:qty,
        _token:'{{csrf_token()}}'
      };

      $.post({
        url:'{{route("cart.update")}}',
        data:data,
        success:function(e){
          if(!e.error){
            if(e.status=!1){
              $('.modal .modal-body .alert .alert-danger').show();
            }else{
              if(e.session.items[id]){
                $sum.text('$' + e.session.items[id]['price']);
                $total.text('$' + e.session.totalPrice);
                $cartBadge.text(e.session.totalQty);
              }
            }
          }
        }
      });
    });
  });
  // ======= END OF WHEN QUANTITY IS CHANGING ======== //

  // ======= WHEN REMOVE BUTTON IS CLICKED ======== //
  $('.my-product-remove').on('click', function(e){
    var id = $(this).closest('tr').attr('data-id');
    var $tr = $(this).closest('tr');
    var $total = $(this).closest('table').find('.my-cart-total b');
    var $cartBadge = $(document).find('.my-cart-icon .my-cart-badge');

    var data = {
      id:id,
      _token:'{{csrf_token()}}'
    };

    $.post({
      url:'{{route("cart.remove")}}',
      data:data,
      success:function(e){
        if(!e.error){
          $total.text('$' + e.session.totalPrice);
          $cartBadge.text(e.session.totalQty);
          $tr.remove();
          if(e.session.items.length<1){
            location.reload(true);
          }
        }
      }
    });
  });
  // ======= END OF WHEN REMOVE BUTTON IS CLICKED ======== //

});
