# README #

Hello, welcome. This is a simple shopping page module made using laravel framework.
It Covers:  
1. Shopping Page  
2. Shopping Cart  
3. Payment Gateway (Using Stripe Integration)  
4. Transaction Invoice / Receipt

### System Requirements ###

* PHP >= 5.6.4
* MariaDB / MySQL Database
* Composer (https://getcomposer.org/)
* Javascript Turned On in your browser
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

### Prerequisite ###

Install Composer (https://getcomposer.org/)
-------------------------------------------

### How do I get set up? ###
###### Please Follow Steps Below in order to install the application
1. Open Terminal
2. Git clone :
	```$ git clone https://rifqi96@bitbucket.org/rifqi96/rdp.git```
3. Change Working Path to ```/rdp```
4. Dependencies Installation :
	```$ composer install```
5. Create a Database with name ```rdp```
6. Configuration:
	1. Open ```.env``` and ```config/database.php```
	2. Change with your database information
7. Database Migration (Please do option 1 for more simple, if error happens, do option 2):
	* **OPTION 1**:
		1. Run Command : ```$ php artisan install:app```
	* **OPTION 2**:
		1. Migrate Database : ```$ php artisan migrate```
		2. Seed Database Table : ```$ php artisan db:seed --class=CreateProductsSeeder```

* Now All Set Up!

### How to run it ? ###
###### There are 2 options in order to run the application
1. Open Terminal
2. Type Command : ```$ php artisan serve```
3. Open the site through following url: ```{localhost}:8000```
	* **Note: {localhost} is your localhost url. Normally *localhost* or *127.0.0.1* **
###### OR
* Open the site through following url: ```{url}/rdp/public```
	* **Note: {url} is your server url.**

###### **Note: Please open ```/documentation``` folder for Flowchart, Class Diagram, ERD and other documentations**

### ERD, Flowchart, Documentations, etc ###

*You can open the files on ```documentation/```*

###### ERD
![alt text](https://bytebucket.org/rifqi96/rdp/raw/33e46c2eb873b01cb4422071369432ca95dcf04a/documentation/RDP%20-%20ERD.png)

###### Flowchart
![alt text](https://bytebucket.org/rifqi96/rdp/raw/ec48e74490351db83f6110459bbbbc75076229f7/documentation/RDP%20-%20Flowchart.png)

###### Class Diagram
![alt text](https://bytebucket.org/rifqi96/rdp/raw/ec48e74490351db83f6110459bbbbc75076229f7/documentation/RDP%20-%20Class%20Diagram.png)

Thank You :)
------------------
