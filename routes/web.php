<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', [
  'uses' => 'ShoppingPageController@index',
  'as' => '/'
]);

Route::prefix('cart')->group(function(){
  Route::post('/add', [
    'uses' => 'CartController@add',
    'as' => 'cart.add'
  ]);

  Route::post('/update', [
    'uses' => 'CartController@update',
    'as' => 'cart.update'
  ]);

  Route::post('/remove', [
    'uses' => 'CartController@remove',
    'as' => 'cart.remove'
  ]);

  Route::get('/destroy', [
    'uses' => 'CartController@destroy',
    'as' => 'cart.destroy'
  ]);
});

Route::prefix('transaction')->group(function(){
  Route::post('/checkout', [
    'uses' => 'TransactionController@checkout',
    'as' => 'transaction.checkout'
  ]);

  Route::prefix('receipt')->group(function(){
    Route::get('/{id}', [
      'uses' => 'ReceiptController@show',
      'as' => 'transaction.receipt'
    ]);
  });

  Route::prefix('add')->group(function(){
    Route::post('/', [
      'uses' => 'TransactionController@add',
      'as' => 'transaction.add'
    ]);

    Route::post('/address',[
      'uses' => 'TransactionController@addAddress',
      'as' => 'transaction.add.address'
    ]);

    Route::post('/customer',[
      'uses' => 'TransactionController@addCustomer',
      'as' => 'transaction.add.customer'
    ]);
  });

});
